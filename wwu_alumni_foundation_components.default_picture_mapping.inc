<?php
/**
 * @file
 * wwu_alumni_foundation_components.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function wwu_alumni_foundation_components_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Action Images';
  $picture_mapping->machine_name = 'action_images';
  $picture_mapping->breakpoint_group = 'action_images';
  $picture_mapping->mapping = array(
    'breakpoints.theme.wwuzen_alumni_foundation.action-large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'medium_9_8__360x320_',
      ),
    ),
    'breakpoints.theme.wwuzen_alumni_foundation.desktop' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'medium_5_2__800x320_',
      ),
    ),
    'breakpoints.theme.wwuzen_alumni_foundation.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'medium_1_1__320x320_',
      ),
    ),
    'breakpoints.theme.wwuzen_alumni_foundation.tablet' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'medium_15_8__600x320_',
      ),
    ),
  );
  $export['action_images'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Event Images';
  $picture_mapping->machine_name = 'event_images';
  $picture_mapping->breakpoint_group = 'event_images';
  $picture_mapping->mapping = array(
    'breakpoints.theme.wwuzen_alumni_foundation.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'large_19_10__1140x600_',
      ),
    ),
  );
  $export['event_images'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Header Image';
  $picture_mapping->machine_name = 'header_image';
  $picture_mapping->breakpoint_group = 'header_image';
  $picture_mapping->mapping = array(
    'breakpoints.theme.wwuzen_alumni_foundation.max-content-width' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => '_original image_',
      ),
    ),
    'breakpoints.theme.wwuzen_alumni_foundation.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'large_19_10__1140x600_',
      ),
    ),
  );
  $export['header_image'] = $picture_mapping;

  return $export;
}
