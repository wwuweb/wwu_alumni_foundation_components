<?php
/**
 * @file
 * wwu_alumni_foundation_components.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wwu_alumni_foundation_components_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'featured_foundation_achiever';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Featured Foundation Achiever';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Featured Image */
  $handler->display->display_options['fields']['field_achiever_featured_image']['id'] = 'field_achiever_featured_image';
  $handler->display->display_options['fields']['field_achiever_featured_image']['table'] = 'field_data_field_achiever_featured_image';
  $handler->display->display_options['fields']['field_achiever_featured_image']['field'] = 'field_achiever_featured_image';
  $handler->display->display_options['fields']['field_achiever_featured_image']['label'] = '';
  $handler->display->display_options['fields']['field_achiever_featured_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_achiever_featured_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_achiever_featured_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['text'] = 'Read more &raquo;';
  $handler->display->display_options['fields']['path']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<h3 class="views-field-title">[title]</h3>
<div class="views-field-body">[body]</div>
<div class="read-more-link">[path]</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foundation_achiever' => 'foundation_achiever',
  );
  /* Filter criterion: Content: Featured (field_achiever_featured) */
  $handler->display->display_options['filters']['field_achiever_featured_value']['id'] = 'field_achiever_featured_value';
  $handler->display->display_options['filters']['field_achiever_featured_value']['table'] = 'field_data_field_achiever_featured';
  $handler->display->display_options['filters']['field_achiever_featured_value']['field'] = 'field_achiever_featured_value';
  $handler->display->display_options['filters']['field_achiever_featured_value']['value'] = array(
    1 => '1',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $export['featured_foundation_achiever'] = $view;

  $view = new view();
  $view->name = 'foundation_achievers';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Foundation Achievers';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Together We Can Achieve Great Things';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '11';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['header']['area_text_custom']['content'] = 'Western students need your help to be successful. Just as you\'ve had mentors and supporters along your life\'s path, student success in today\'s socioeconomic and political landscape requires support from everyone: families, communities, alumni, donors, organizations, businesses and Western. Find out how you can participate in supporting each of the areas and colleges below.';
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'foundation_achievers' => 'foundation_achievers',
    'news' => 0,
    'stories' => 0,
    'board_of_directors' => 0,
    'ua_staff_office_of_vp_for_ua' => 0,
    'ua_staff_advancement_services' => 0,
    'ua_staff_alumni_relations_divisi' => 0,
    'ua_staff_alumni_relations' => 0,
    'ua_staff_communications_marketin' => 0,
    'ua_staff_annual_fund_special_eve' => 0,
    'ua_major_gift_team' => 0,
    'ua_staff_coporate_and_foundation' => 0,
    'ua_staff_advancment_services_no_' => 0,
    'ua_staff_major_gift_team_no_port' => 0,
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_achiever_photo']['id'] = 'field_achiever_photo';
  $handler->display->display_options['fields']['field_achiever_photo']['table'] = 'field_data_field_achiever_photo';
  $handler->display->display_options['fields']['field_achiever_photo']['field'] = 'field_achiever_photo';
  $handler->display->display_options['fields']['field_achiever_photo']['label'] = '';
  $handler->display->display_options['fields']['field_achiever_photo']['element_class'] = 'circular-image-container';
  $handler->display->display_options['fields']['field_achiever_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_achiever_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_achiever_photo']['settings'] = array(
    'image_style' => 'medium_1_1__320x320_',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Sort criterion: Nodequeue: Position */
  $handler->display->display_options['sorts']['position']['id'] = 'position';
  $handler->display->display_options['sorts']['position']['table'] = 'nodequeue_nodes';
  $handler->display->display_options['sorts']['position']['field'] = 'position';
  $handler->display->display_options['sorts']['position']['relationship'] = 'nodequeue_rel';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foundation_achiever' => 'foundation_achiever',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $export['foundation_achievers'] = $view;

  return $export;
}
