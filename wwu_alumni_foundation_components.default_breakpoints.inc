<?php
/**
 * @file
 * wwu_alumni_foundation_components.default_breakpoints.inc
 */

/**
 * Implements hook_default_breakpoints().
 */
function wwu_alumni_foundation_components_default_breakpoints() {
  $export = array();

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_alumni_foundation.action-large';
  $breakpoint->name = 'action-large';
  $breakpoint->breakpoint = '(min-width: 1060px)';
  $breakpoint->source = 'wwuzen_alumni_foundation';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 2;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_alumni_foundation.action-large'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_alumni_foundation.desktop';
  $breakpoint->name = 'desktop';
  $breakpoint->breakpoint = '(min-width: 801px)';
  $breakpoint->source = 'wwuzen_alumni_foundation';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 3;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_alumni_foundation.desktop'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_alumni_foundation.max-content-width';
  $breakpoint->name = 'max-content-width';
  $breakpoint->breakpoint = '(min-width: 1140px)';
  $breakpoint->source = 'wwuzen_alumni_foundation';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 1;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_alumni_foundation.max-content-width'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_alumni_foundation.mobile';
  $breakpoint->name = 'mobile';
  $breakpoint->breakpoint = '(min-width: 320px)';
  $breakpoint->source = 'wwuzen_alumni_foundation';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 4;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_alumni_foundation.mobile'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_alumni_foundation.tablet';
  $breakpoint->name = 'tablet';
  $breakpoint->breakpoint = '(min-width: 600px)';
  $breakpoint->source = 'wwuzen_alumni_foundation';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 5;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_alumni_foundation.tablet'] = $breakpoint;

  return $export;
}
