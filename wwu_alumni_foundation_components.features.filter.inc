<?php
/**
 * @file
 * wwu_alumni_foundation_components.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function wwu_alumni_foundation_components_filter_default_formats() {
  $formats = array();

  // Exported format: Clean HTML.
  $formats['clean_html'] = array(
    'format' => 'clean_html',
    'name' => 'Clean HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => -9,
    'filters' => array(
      'media_filter' => array(
        'weight' => -50,
        'status' => 1,
        'settings' => array(),
      ),
      'wwu_img_caption' => array(
        'weight' => -42,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_autop' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => -39,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'alumnidev.wwu.edu
alumni.wwu.edu',
          'protocol_style' => 'proto-rel',
          'local_paths_exploded' => array(
            0 => array(
              'path' => 'alumnidev.wwu.edu',
            ),
            1 => array(
              'path' => 'alumni.wwu.edu',
            ),
            2 => array(
              'path' => '/alumni/',
            ),
            3 => array(
              'path' => '/alumni/',
              'host' => 'localhost',
            ),
          ),
          'base_url_host' => 'localhost',
        ),
      ),
    ),
  );

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 0,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'node_embed' => array(
        'weight' => -50,
        'status' => 1,
        'settings' => array(),
      ),
      'media_filter' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(),
      ),
      'wwu_img_caption' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'alumnidev.wwu.edu
alumni.wwu.edu',
          'protocol_style' => 'proto-rel',
        ),
      ),
      'filter_autop' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
