<?php
/**
 * @file
 * wwu_alumni_foundation_components.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function wwu_alumni_foundation_components_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__header__file_field_picture';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'picture_mapping' => 'header_image',
    'fallback_image_style' => '',
    'lazyload' => 0,
    'lazyload_aspect_ratio' => 0,
    'image_link' => '',
  );
  $export['image__header__file_field_picture'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__header__oembed';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '1280',
    'height' => '720',
    'wmode' => 'transparent',
  );
  $export['video__header__oembed'] = $file_display;

  return $export;
}
