<?php
/**
 * @file
 * wwu_alumni_foundation_components.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wwu_alumni_foundation_components_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_views_api().
 */
function wwu_alumni_foundation_components_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function wwu_alumni_foundation_components_image_default_styles() {
  $styles = array();

  // Exported image style: call_out.
  $styles['call_out'] = array(
    'label' => 'Call Out',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 675,
          'height' => 375,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: event_view_large_1_1__650x650_.
  $styles['event_view_large_1_1__650x650_'] = array(
    'label' => 'Event View Large 1:1 (650x650)',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 650,
          'height' => 650,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: event_view_medium_1_1__480x480_.
  $styles['event_view_medium_1_1__480x480_'] = array(
    'label' => 'Event View Medium 1:1 (480x480)',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: large_19_10__1140x600_.
  $styles['large_19_10__1140x600_'] = array(
    'label' => 'Large 19:10 (1140x600)',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1140,
          'height' => 600,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_15_8__600x320_.
  $styles['medium_15_8__600x320_'] = array(
    'label' => 'Medium 15:8 (600x320)',
    'effects' => array(
      8 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_19_10__570x300_.
  $styles['medium_19_10__570x300_'] = array(
    'label' => 'Medium 19:10 (570x300)',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 570,
          'height' => 300,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_1_1__320x320_.
  $styles['medium_1_1__320x320_'] = array(
    'label' => 'Medium 1:1 (320x320)',
    'effects' => array(
      15 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_2_1__600x300_.
  $styles['medium_2_1__600x300_'] = array(
    'label' => 'Medium 2:1 (600x300)',
    'effects' => array(
      9 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 300,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: medium_3_1__1080x360_.
  $styles['medium_3_1__1080x360_'] = array(
    'label' => 'Medium 3:1 (1080x360)',
    'effects' => array(
      10 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1080,
          'height' => 360,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: medium_3_1__600x200_.
  $styles['medium_3_1__600x200_'] = array(
    'label' => 'Medium 3:1 (600x200)',
    'effects' => array(
      14 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 200,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: medium_5_2__800x320_.
  $styles['medium_5_2__800x320_'] = array(
    'label' => 'Medium 5:2 (800x320)',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: medium_9_8__360x320_.
  $styles['medium_9_8__360x320_'] = array(
    'label' => 'Medium 9:8 (360x320)',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 360,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: news_view_1_1_medium__200x200_.
  $styles['news_view_1_1_medium__200x200_'] = array(
    'label' => 'News View 1:1 Medium (200x200)',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 200,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: news_view_2_3_1_large__600x200_.
  $styles['news_view_2_3_1_large__600x200_'] = array(
    'label' => 'News View 2.3:1 Large (600x200)',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 200,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function wwu_alumni_foundation_components_node_info() {
  $items = array(
    'action' => array(
      'name' => t('Action'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'alumni_star' => array(
      'name' => t('Alumni Star'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'foundation_achiever' => array(
      'name' => t('Foundation Achiever'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'image_bar' => array(
      'name' => t('Image Bar'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'quotation' => array(
      'name' => t('Quotation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'staff_board_members' => array(
      'name' => t('Staff & Board Members'),
      'base' => 'node_content',
      'description' => t('The staff and board members content type is used to populate views that show who is a board member and who is a staff member. '),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'story_callout' => array(
      'name' => t('Story Callout'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'viking_fact' => array(
      'name' => t('Viking Fact'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
