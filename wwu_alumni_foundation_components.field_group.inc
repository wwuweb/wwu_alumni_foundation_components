<?php
/**
 * @file
 * wwu_alumni_foundation_components.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wwu_alumni_foundation_components_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_board_member|node|staff_board_members|form';
  $field_group->group_name = 'group_board_member';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'staff_board_members';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Board Member',
    'weight' => '3',
    'children' => array(
      0 => 'field_western_graduation_year',
      1 => 'field_employer',
      2 => 'field_current_job_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-board-member field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_board_member|node|staff_board_members|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|page|form';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '5',
    'children' => array(
      0 => 'field_header',
      1 => 'field_disable_header',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-header field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_header|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_staff_details|node|staff_board_members|form';
  $field_group->group_name = 'group_staff_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'staff_board_members';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Staff',
    'weight' => '6',
    'children' => array(
      0 => 'field_email_address',
      1 => 'field_phone_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-staff-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_staff_details|node|staff_board_members|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Board Member');
  t('Header');
  t('Staff');

  return $field_groups;
}
