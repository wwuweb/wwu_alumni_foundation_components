<?php
/**
 * @file
 * wwu_alumni_foundation_components.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function wwu_alumni_foundation_components_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'action_images';
  $breakpoint_group->name = 'Action Images';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.wwuzen_alumni_foundation.action-large',
    1 => 'breakpoints.theme.wwuzen_alumni_foundation.desktop',
    2 => 'breakpoints.theme.wwuzen_alumni_foundation.mobile',
    3 => 'breakpoints.theme.wwuzen_alumni_foundation.tablet',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['action_images'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'event_images';
  $breakpoint_group->name = 'Event Images';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.wwuzen_alumni_foundation.mobile',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['event_images'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'header_image';
  $breakpoint_group->name = 'Header Image';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.wwuzen_alumni_foundation.max-content-width',
    1 => 'breakpoints.theme.wwuzen_alumni_foundation.mobile',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['header_image'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'wwuzen_alumni_foundation';
  $breakpoint_group->name = 'WWUZEN Alumni Foundation';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.wwuzen_alumni_foundation.max-content-width',
    1 => 'breakpoints.theme.wwuzen_alumni_foundation.action-large',
    2 => 'breakpoints.theme.wwuzen_alumni_foundation.desktop',
    3 => 'breakpoints.theme.wwuzen_alumni_foundation.tablet',
    4 => 'breakpoints.theme.wwuzen_alumni_foundation.mobile',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['wwuzen_alumni_foundation'] = $breakpoint_group;

  return $export;
}
